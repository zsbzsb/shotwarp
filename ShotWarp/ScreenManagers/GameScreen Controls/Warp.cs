﻿using System;
using SFML.Window;
using SFML.Graphics;
using Spitfire.UI;
using Spitfire.Resources;
using NetEXT.TimeFunctions;
using NetEXT.MathFunctions;

namespace ShotWarp.ScreenManagers.GameScreen_Controls
{
    public class Warp : ControlBase
    {
        #region Consts
        private static readonly Time FrameTime = Time.FromMilliseconds(100);
        private static readonly int MaxFrames = 3;
        #endregion

        #region Variables
        private Texture _backgroundtexture = null;
        private Sprite _backgroundsprite = null;
        private Time _animatetime = Time.Zero;
        private int _anitmateoffset = RandomGenerator.Random(0, 2);
        private Time _warptime = Time.Zero;
        private Text _countdowntime = null;
        private bool _showtime = true;
        #endregion

        #region Properties
        public Time WarpTime
        {
            get
            {
                return _warptime;
            }
            set
            {
                _warptime = value;
            }
        }
        public float Rotation
        {
            get
            {
                return _backgroundsprite.Rotation;
            }
        }
        public bool ShowTime
        {
            get
            {
                return _showtime;
            }
            set
            {
                _showtime = value;
            }
        }
        #endregion

        #region Constructors
        public Warp(ResourceManager Manager, float Rotation)
        {
            Size = new Vector2f(360, 26);
            Enabled = false;
            _backgroundtexture = Manager.GetTexture(@".\\Game\Warp.png");
            _backgroundsprite = new Sprite(_backgroundtexture);
            _backgroundsprite.Origin = new Vector2f(0, 13);
            _backgroundsprite.Rotation = Rotation;
            _backgroundsprite.TextureRect = new IntRect(0, _anitmateoffset * 26, 360, 26);
            _countdowntime = new Text("", Manager.GetFont(@".\\Fonts\main.ttf")) { Color = Color.Blue };
            _countdowntime.CharacterSize = 18;
            _countdowntime.Rotation = _backgroundsprite.Rotation;
            float aangle = _backgroundsprite.Rotation;
            while (aangle < 0) { aangle += 360; }
            while (aangle > 360) { aangle -= 360; }
            if (aangle > 90 && aangle < 270)
            {
                _countdowntime.Scale = new Vector2f(-1, -1);
                _countdowntime.Origin = new Vector2f(225, 13);
            }
            else
            {
                _countdowntime.Scale = new Vector2f(1, 1);
                _countdowntime.Origin = new Vector2f(-135, 13);
            }
        }
        #endregion

        #region Functions
        public override void Update(Time DeltaTime)
        {
            _animatetime += DeltaTime;
            bool updatesprite = false;
            while (_animatetime >= FrameTime)
            {
                _animatetime -= FrameTime;
                updatesprite = true;
                _anitmateoffset += 1;
                if (_anitmateoffset > MaxFrames - 1) _anitmateoffset = 0;
            }
            if (updatesprite) _backgroundsprite.TextureRect = new IntRect(0, _anitmateoffset * 26, 360, 26);
            base.Update(DeltaTime);
        }
        public override void Draw(RenderTarget Target, RenderStates States)
        {
            _backgroundsprite.Position = Position;
            Target.Draw(_backgroundsprite, States);
            if (_showtime)
            {
                _countdowntime.Position = Position;
                _countdowntime.DisplayedString = Math.Round(WarpTime.Seconds, 0).ToString() + " seconds";
                Target.Draw(_countdowntime, States);
            }
            base.Draw(Target, States);
        }
        #endregion
    }
}

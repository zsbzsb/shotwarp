﻿using System;
using SFML.Window;
using SFML.Graphics;
using Spitfire.UI;
using Spitfire.Resources;

namespace ShotWarp.ScreenManagers.GameScreen_Controls
{
    public class Background : ControlBase
    {
        #region Variables
        private Texture _backgroundtexture = null;
        private Sprite _backgroundsprite = null;
        #endregion

        #region Constructors
        public Background(ResourceManager Manager)
        {
            Size = new Vector2f(700, 600);
            Enabled = false;
            _backgroundtexture = Manager.GetTexture(@".\\Game\Game Background.png");
            _backgroundsprite = new Sprite(_backgroundtexture);
        }
        #endregion

        #region Functions
        public override void Draw(RenderTarget Target, RenderStates States)
        {
            _backgroundsprite.Position = Position;
            Target.Draw(_backgroundsprite, States);
            base.Draw(Target, States);
        }
        #endregion
    }
}

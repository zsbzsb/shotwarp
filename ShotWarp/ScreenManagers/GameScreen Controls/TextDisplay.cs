﻿using System;
using SFML.Window;
using SFML.Graphics;
using Spitfire.UI;
using Spitfire.Resources;

namespace ShotWarp.ScreenManagers.GameScreen_Controls
{
    public class TextDisplay : ControlBase
    {
        #region Variables
        private Text _displaytext = null;
        #endregion

        #region Properties
        public string Text
        {
            get
            {
                return _displaytext.DisplayedString;
            }
            set
            {
                _displaytext.DisplayedString = value;
            }
        }
        public Color Color
        {
            get
            {
                return _displaytext.Color;
            }
            set
            {
                _displaytext.Color = value;
            }
        }
        public uint FontSize
        {
            get
            {
                return _displaytext.CharacterSize;
            }
            set
            {
                _displaytext.CharacterSize = value;
            }
        }
        public float Rotation
        {
            get
            {
                return _displaytext.Rotation;
            }
            set
            {
                _displaytext.Rotation = value;
            }
        }
        #endregion

        #region Constructors
        public TextDisplay(ResourceManager Manager)
        {
            Size = new Vector2f(1, 1);
            Enabled = false;
            _displaytext = new Text("", Manager.GetFont(@".\\Fonts\main.ttf"));
        }
        #endregion

        #region Functions
        public override void Draw(RenderTarget Target, RenderStates States)
        {
            _displaytext.Position = Position;
            Target.Draw(_displaytext, States);
            base.Draw(Target, States);
        }
        #endregion
    }
}

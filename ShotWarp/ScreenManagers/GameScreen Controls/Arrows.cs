﻿using System;
using System.Collections.Generic;
using SFML.Window;
using SFML.Graphics;
using Spitfire.UI;
using Spitfire.Resources;
using NetEXT.TimeFunctions;

namespace ShotWarp.ScreenManagers.GameScreen_Controls
{
    public class Arrows : ControlBase
    {
        #region Variables
        private Texture _arrowtexture = null;
        private Sprite _arrowsprite = null;
        private List<Arrow> _arrows = new List<Arrow>();
        private RectangleShape _overlay = new RectangleShape() { FillColor = new Color(155, 155, 155, 127), Size = new Vector2f(70, 15), Origin = new Vector2f(35, 7.5f) };
        private Text _countdowntime = null;
        #endregion

        #region Properties
        public Arrow[] ArrowsList
        {
            get
            {
                return _arrows.ToArray();
            }
        }
        #endregion

        #region Constructors
        public Arrows(ResourceManager Manager)
        {
            Size = new Vector2f(700, 600);
            Enabled = false;
            _arrowtexture = Manager.GetTexture(@".\\Game\Arrow.png");
            _arrowsprite = new Sprite(_arrowtexture);
            _arrowsprite.Origin = new Vector2f(70, 15);
            _arrowsprite.Scale = new Vector2f(.5f, .5f);
            _countdowntime = new Text("", Manager.GetFont(@".\\Fonts\main.ttf")) { Color = Color.Black };
        }
        #endregion

        #region Functions
        public void AddArrow(Arrow Arrow)
        {
            _arrows.Add(Arrow);
        }
        public void RemoveArrow(Arrow Arrow)
        {
            _arrows.Remove(Arrow);
        }
        public override void Draw(RenderTarget Target, RenderStates States)
        {
            foreach (var arrow in _arrows)
            {
                _arrowsprite.Position = arrow.Position;
                _arrowsprite.Rotation = arrow.Velocity.A;
                if (arrow.State == ArrowState.SuspendedLeft || arrow.State == ArrowState.SuspendedRight || arrow.State == ArrowState.SuspendedSpace)
                {
                    _arrowsprite.Color = new Color(255, 255, 255, 127);
                    Target.Draw(_arrowsprite, States);
                    _overlay.Position = arrow.Position;
                    _overlay.Rotation = arrow.Velocity.A;
                    Target.Draw(_overlay, States);
                    _countdowntime.CharacterSize = 18;
                    _countdowntime.Position = arrow.Position;
                    _countdowntime.Origin = new Vector2f(35, 7.5f);
                    _countdowntime.Rotation = arrow.Velocity.A;
                    _countdowntime.DisplayedString = Math.Round(arrow.RemainingTime.Seconds, 0).ToString();
                    float aangle = arrow.Velocity.A;
                    while (aangle < 0) { aangle += 360; }
                    while (aangle > 360) { aangle -= 360; }
                    if (aangle > 90 && aangle < 270) _countdowntime.Scale = new Vector2f(-1, -1);
                    else _countdowntime.Scale = new Vector2f(1, 1);
                    Target.Draw(_countdowntime, States);
                }
                else
                {
                    _arrowsprite.Color = new Color(255, 255, 255, 255);
                    Target.Draw(_arrowsprite, States);
                }
            }
            base.Draw(Target, States);
        }
        #endregion
    }
}

﻿using System;
using SFML.Window;
using SFML.Graphics;
using Spitfire.UI;
using Spitfire.Resources;

namespace ShotWarp.ScreenManagers.GameScreen_Controls
{
    public class Globe : ControlBase
    {
        #region Variables
        private Texture _backgroundtexture = null;
        private Sprite _backgroundsprite = null;
        #endregion

        #region Constructors
        public Globe(ResourceManager Manager)
        {
            Size = new Vector2f(65, 65);
            Enabled = false;
            _backgroundtexture = Manager.GetTexture(@".\\Game\Globe.png");
            _backgroundsprite = new Sprite(_backgroundtexture);
        }
        #endregion

        #region Functions
        public override void Draw(RenderTarget Target, RenderStates States)
        {
            _backgroundsprite.Position = Position;
            Target.Draw(_backgroundsprite, States);
            base.Draw(Target, States);
        }
        #endregion
    }
}

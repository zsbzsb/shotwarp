﻿using System;
using SFML.Window;
using SFML.Graphics;
using Spitfire.UI;
using Spitfire.UI.Controls;
using Spitfire.ScreenManager;
using Spitfire.Resources;
using NetEXT.TimeFunctions;
using ShotWarp.ScreenManagers.GameScreen_Controls;

namespace ShotWarp.ScreenManagers
{
    public class MainMenu : ScreenManagerUIBase
    {
        #region Variables
        private Button newgamebutton = null;
        private Arrows _arrowscontrol = null;
        private Targets _targetscontrol = null;
        private GravityDrawing _gravitycontrol = null;
        private Warp _leftwarp = null;
        private Warp _rightwarp = null;
        private Warp _spaceleft = null;
        private Warp _spaceright = null;
        private Texture _zsbtexture = null;
        private Sprite _zsbsprite = null;
        private TextDisplay _writtenby = null;
        private TextDisplay _audioby = null;
        #endregion

        #region Contructors
        public MainMenu(Vector2f CurrentScreenSize)
            : base(CurrentScreenSize, new ResourceManager(@".\\Resources\PR.vfs"))
        {
            AudioManager.Manager = ResourceManager;
            MusicManager.Manager = ResourceManager;
            MusicManager.QueueSong(@".\\Music\back1.ogg");
            MusicManager.QueueSong(@".\\Music\back2.ogg");
            MusicManager.QueueSong(@".\\Music\back3.ogg");
            MusicManager.QueueSong(@".\\Music\back4.ogg");
            MusicManager.QueueSong(@".\\Music\back5.ogg");
            MusicManager.QueueSong(@".\\Music\back6.ogg");
            MusicManager.QueueSong(@".\\Music\back7.ogg");
            MusicManager.Play();
            _zsbtexture = ResourceManager.GetTexture(@".\\Menu\zsb.png");
            _zsbsprite = new Sprite(_zsbtexture) { Scale = new Vector2f(.125f, .125f) };
            _zsbsprite.Position = new Vector2f(50, 475);
        }
        #endregion

        #region Functions
        protected override void BuildUI()
        {
            newgamebutton = UIFactory.CreateButton("New Game", ResourceManager);
            newgamebutton.ButtonClicked += StartGame;
            newgamebutton.Position = new Vector2f(250, 200);
            newgamebutton.ZOrder = 15;
            AddControl(newgamebutton);
            AddControl(new Background(ResourceManager) { ZOrder = -1 });
            _gravitycontrol = new GravityDrawing(ResourceManager);
            AddControl(_gravitycontrol);
            _rightwarp = new Warp(ResourceManager, -123.6f) { Position = new Vector2f(550, 600), ShowTime = false };
            AddControl(_rightwarp);
            _leftwarp = new Warp(ResourceManager, -56.3f) { Position = new Vector2f(150, 600), ShowTime = false };
            AddControl(_leftwarp);
            _spaceright = new Warp(ResourceManager, 123.6f) { Position = new Vector2f(554.43f, -6.66f), ShowTime = false };
            AddControl(_spaceright);
            _spaceleft = new Warp(ResourceManager, 56.3f) { Position = new Vector2f(145.56f, -6.66f), ShowTime = false };
            AddControl(_spaceleft);
            _targetscontrol = new Targets(ResourceManager) { ZOrder = 1 };
            AddControl(_targetscontrol);
            _arrowscontrol = new Arrows(ResourceManager) { ZOrder = 6 };
            AddControl(_arrowscontrol);
            AddControl(new Globe(ResourceManager) { Position = new Vector2f(317.5f, 267.5f), ZOrder = 10 });
            AddControl(new Ground(ResourceManager) { Position = new Vector2f(0, 592), ZOrder = 10 });
            _writtenby = new TextDisplay(ResourceManager) { FontSize = 15, Text = "   Written By\n\n\n\n\n\n\n\nZachariah Brown", Color = Color.Black, ZOrder = 15, Rotation = -56.3f, Position = new Vector2f(13, 515) };
            AddControl(_writtenby);
            _audioby = new TextDisplay(ResourceManager) { FontSize = 15, Text = "   Music By\nElijah Brown", Color = Color.Black, ZOrder = 15, Rotation = 56.3f, Position = new Vector2f(525, 460) };
            AddControl(_audioby);

            _gravitycontrol.AddGravity(new Gravity(new Vector2f(70, 200)));
            _gravitycontrol.AddGravity(new Gravity(new Vector2f(600, 450)) { Type = GravityType.Push });

            _targetscontrol.AddTarget(new Target(new Vector2f(340, 35), TargetType.Space, Time.Zero, Time.Zero) { State = TargetState.Alive });
        }
        private void StartGame(Button Sender)
        {
            OnSwitchScreen(new GameScreen(ScreenSize, ResourceManager));
        }
        public override void Draw(RenderTarget Target)
        {
            base.Draw(Target);
            Target.Draw(_zsbsprite);
        }
        #endregion
    }
}

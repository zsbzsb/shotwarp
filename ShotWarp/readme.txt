﻿ShotWarp

Written by Zachariah Brown with music by Elijah Brown (thanks!).

ShotWarp was written for the 2nd SFML Game Jam with the theme
"Time Travel". The basic idea of the game is to shoot arrows
into the targets and meet the quota without running out of ammo.

Once you click "New Game" you will be in the game screen on
level one. On the left side you will see times past, on the
right side is the future. In the bottom middle triangle is
the current time. On the top side is space time, space time
is a void in the space time continumn that is outside time.

To launch an arrow click in the present time area and while
holding down your mouse drag in the direction you wish to fire.
When you release the mouse an arrow will shoot in the direction
you aimed. As the arrow passes through the warp zone into
either the past or future zones the arrow will be temporarily
suspended in the space time continumn. As soon as the warp is
completed your arrow will continue on its path.

There is also gravity wells, these can either push or pull
your arrow. You can easily determine the type of well as
gravity wells that push pulse outwards and wells that pull
pulse inwards.

In order to complete the level you must hit the minimum
number of targets in each time area. At the top of the
screen there will be a display showing the required number
of hits for each time area and the current total hits.

Thank you for trying ShotWarp and have a great day!
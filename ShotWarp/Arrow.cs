﻿using System;
using SFML.Window;
using NetEXT.Vectors;
using NetEXT.TimeFunctions;
using Spitfire.Resources;

namespace ShotWarp
{
    public class Arrow
    {
        #region Variables
        private ArrowState _state = ArrowState.Launched;
        private PolarVector _velocity = new PolarVector();
        private Vector2f _position = new Vector2f();
        private Time _suspendtime = Time.Zero;
        private Time _elapsedtime = Time.Zero;
        #endregion

        #region Properties
        public Time RemainingTime
        {
            get
            {
                return _suspendtime - _elapsedtime;
            }
        }
        public ArrowState State
        {
            get
            {
                return _state;
            }
            set
            {
                _state = value;
            }
        }
        public Vector2f Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
            }
        }
        public PolarVector Velocity
        {
            get
            {
                return _velocity;
            }
            set
            {
                _velocity = value;
            }
        }
        public Time SuspendTime
        {
            get
            {
                return _suspendtime;
            }
            set
            {
                _suspendtime = value;
            }
        }
        #endregion

        #region Events
        public event Action<Arrow> Dead;
        #endregion

        #region Constructors
        public Arrow(float VelocityScale, float Angle, Vector2f Position)
        {
            _velocity = new PolarVector(VelocityScale * 175f, Angle);
            _position = Position;
        }
        #endregion

        #region Functions
        public void Kill()
        {
            _state = ArrowState.Dead;
            if (Dead != null) Dead(this);
        }
        public void Update(Time DeltaTime, Gravity GravitySource)
        {
            if (_state != ArrowState.SuspendedLeft && _state != ArrowState.SuspendedRight && _state != ArrowState.SuspendedSpace && _state != ArrowState.Dead)
            {
                if (GravitySource != null)
                {
                    float gangle = TrigHelper.Angle(_position, GravitySource.Position);
                    float aangle = _velocity.A;
                    float diff = 0;
                    if (GravitySource.Type == GravityType.Push) diff = 180;
                    if (Math.Abs(gangle - aangle - diff) >= 5f)
                    {
                        while (aangle < 0) { aangle += 360; }
                        while (aangle > 360) { aangle -= 360; }
                        while (gangle < 0) { gangle += 360; }
                        while (gangle > 360) { gangle -= 360; }
                        if (GravitySource.Type == GravityType.Pull)
                        {
                            if ((aangle - gangle + 360f) % 360f > 180f) _velocity.A += GravitySource.Strength * (float)DeltaTime.Seconds;
                            else _velocity.A -= GravitySource.Strength * (float)DeltaTime.Seconds;
                        }
                        else if (GravitySource.Type == GravityType.Push)
                        {
                            if ((aangle - gangle + 360f) % 360f > 180f) _velocity.A -= GravitySource.Strength * (float)DeltaTime.Seconds;
                            else _velocity.A += GravitySource.Strength * (float)DeltaTime.Seconds;
                        }
                    }
                }
                _position += (Vector2f)_velocity * (float)DeltaTime.Seconds;
            }
            else
            {
                _elapsedtime += DeltaTime;
                if (_state == ArrowState.SuspendedLeft || _state == ArrowState.SuspendedRight || _state == ArrowState.SuspendedSpace)
                {
                    if (_elapsedtime >= _suspendtime)
                    {
                        _elapsedtime = Time.Zero;
                        if (_state == ArrowState.SuspendedLeft) _state = ArrowState.FlyingLeft;
                        else if (_state == ArrowState.SuspendedRight) _state = ArrowState.FlyingRight;
                        else if (_state == ArrowState.SuspendedSpace) _state = ArrowState.FlyingSpace;
                        AudioManager.Play(@".\\Sound\warp leave.ogg");
                    }
                }
                else if (_state == ArrowState.Dead)
                {
                    if (_elapsedtime >= Time.FromSeconds(3))
                    {
                        if (Dead != null) Dead(this);
                    }
                }
            }
        }
        #endregion
    }
}

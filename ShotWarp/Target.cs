﻿using System;
using System.Collections.Generic;
using SFML.Window;
using SFML.Graphics;
using NetEXT.TimeFunctions;

namespace ShotWarp
{
    public class Target
    {
        #region Variables
        private Vector2f _position = new Vector2f();
        private TargetState _state = TargetState.Prepared;
        private TargetType _type = TargetType.Left;
        private Time _elapsedtime = Time.Zero;
        private Time _preparetime = Time.Zero;
        private Time _livetime = Time.Zero;
        private FloatRect _boundingbox = new FloatRect();
        private List<Arrow> _stuckarrows = new List<Arrow>();
        #endregion

        #region Properties
        public Vector2f Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
            }
        }
        public TargetState State
        {
            get
            {
                return _state;
            }
            set{
                _state = value;
            }
        }
        public TargetType Type
        {
            get
            {
                return _type;
            }
        }
        public Time RemainingTime
        {
            get
            {
                if (_state == TargetState.Prepared) return _preparetime - _elapsedtime;
                else return (_livetime - _elapsedtime);
            }
        }
        #endregion

        #region Events
        public event Action<Target> Dead;
        #endregion

        #region Constructors
        public Target(Vector2f Position, TargetType Type, Time PrepareTime, Time LiveTime)
        {
            _position = Position;
            _type = Type;
            _preparetime = PrepareTime;
            _livetime = LiveTime;
            if (Type == TargetType.Left || Type == TargetType.Right)
            {
                _boundingbox = new FloatRect(Position.X - 20f, Position.Y - 25f, 40, 50);
            }
            else if (Type == TargetType.Space)
            {
                _boundingbox = new FloatRect(Position.X - 25f, Position.Y - 20f, 50, 40);
            }
        }
        #endregion

        #region Functions
        public void StickArrow(Arrow Arrow)
        {
            _stuckarrows.Add(Arrow);
            Arrow.Dead += (arw) => { if (_stuckarrows.Contains(arw)) _stuckarrows.Remove(arw); };
        }
        public void Move(Vector2f Movement)
        {
            Position += Movement;
            foreach (var arrow in _stuckarrows)
            {
                arrow.Position += Movement;
            }
            if (Type == TargetType.Left || Type == TargetType.Right)
            {
                _boundingbox = new FloatRect(Position.X - 20f, Position.Y - 25f, 40, 50);
            }
            else if (Type == TargetType.Space)
            {
                _boundingbox = new FloatRect(Position.X - 25f, Position.Y - 20f, 50, 40);
            }
        }
        public void Update(Time DeltaTime)
        {
            _elapsedtime += DeltaTime;
            if (_state == TargetState.Prepared && _elapsedtime >= _preparetime)
            {
                _state = TargetState.Alive;
                _elapsedtime = Time.Zero;
            }
            else if (_state == TargetState.Alive && _elapsedtime >= _livetime && _livetime.Ticks > 0)
            {
                if (Dead != null) Dead(this);
                while (_stuckarrows.Count > 0)
                {
                    _stuckarrows[0].Kill();
                }
            }
        }
        public bool Hit(Arrow Arrow)
        {
            return (_boundingbox.Contains(Arrow.Position.X, Arrow.Position.Y));
        }
        #endregion
    }
}

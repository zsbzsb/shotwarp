﻿using System;

namespace ShotWarp
{
    public enum TargetType
    {
        Left,
        Right,
        Space
    }
}

﻿using System;

namespace ShotWarp
{
    public enum ArrowState
    {
        Launched,
        SuspendedLeft,
        SuspendedRight,
        SuspendedSpace,
        FlyingLeft,
        FlyingRight,
        FlyingSpace,
        Dead
    }
}

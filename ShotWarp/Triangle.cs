﻿using System;
using SFML.Window;
using NetEXT.Vectors;

namespace ShotWarp
{
    public class Triangle
    {
        #region Variables
        private Vector2f _referencepoint;
        private Vector2f _leftpoint;
        private Vector2f _rightpoint;
        #endregion

        #region Properties
        public Vector2f ReferencePoint
        {
            get
            {
                return _referencepoint;
            }
            set
            {
                _referencepoint = value;
            }
        }
        public Vector2f LeftPoint
        {
            get
            {
                return _leftpoint;
            }
            set
            {
                _leftpoint = value;
            }
        }
        public Vector2f RightPoint
        {
            get
            {
                return _rightpoint;
            }
            set
            {
                _rightpoint = value;
            }
        }
        #endregion

        #region Constructors
        public Triangle(Vector2f ReferencePoint, Vector2f LeftPoint, Vector2f RightPoint)
        {
            _referencepoint = ReferencePoint;
            _leftpoint = LeftPoint;
            _rightpoint = RightPoint;
        }
        #endregion

        #region Functions
        public bool ContainsPoint(float X, float Y)
        {
            Vector2f p = new Vector2f(X, Y);
            if (isSameSide(p, ReferencePoint, LeftPoint, RightPoint) && isSameSide(p, LeftPoint, ReferencePoint, RightPoint) && isSameSide(p, RightPoint, ReferencePoint, LeftPoint)) return true;
            return false;
        }
        private bool isSameSide(Vector2f p1, Vector2f p2, Vector2f a, Vector2f b)
        {
            float cp1 = Vector2Algebra.CrossProduct(b - a, p1 - a);
            float cp2 = Vector2Algebra.CrossProduct(b - a, p2 - a);
            if (Vector2Algebra.DotProduct(new Vector2f(cp1, cp1), new Vector2f(cp2, cp2)) >= 0) return true;
            return false;
        }
        #endregion
    }
}

﻿using System;
using SFML.Window;
using NetEXT.TimeFunctions;
using NetEXT.MathFunctions;

namespace ShotWarp.Levels
{
    public class Level9 : Level
    {
        #region Variables
        private Time _leftrespawn = Time.Zero;
        private Time _rightrespawn = Time.Zero;
        private bool _isspace = false;
        private Gravity _gravity = null;
        private Time _switchtimer = Time.Zero;
        #endregion

        #region Properties
        public override int RequiredLeftTargets
        {
            get
            {
                return 6;
            }
        }
        public override int RequiredRightTargets
        {
            get
            {
                return 6;
            }
        }
        public override int RequiredSpaceTargets
        {
            get
            {
                return 0;
            }
        }
        public override int TotalAmmo
        {
            get
            {
                return 20;
            }
        }
        #endregion

        #region Functions
        public override void Initialize()
        {
            OnSetLeftWarpTime(Time.FromSeconds(3));
            OnSetRightWarpTime(Time.FromSeconds(3));
            _switchtimer = Time.FromSeconds(RandomGenerator.Random(10, 15));
            _gravity = new Gravity(new Vector2f(350, 20), 25) { Type = GravityType.Push };
            OnSetGravityState(GravityState.Space);
            OnSetSpaceGravity(_gravity);
        }
        public override void Update(Time DeltaTime)
        {
            _leftrespawn -= DeltaTime;
            _rightrespawn -= DeltaTime;
            if (_leftrespawn <= Time.Zero)
            {
                _leftrespawn = Time.FromSeconds(RandomGenerator.Random(8, 15));
                if (RandomGenerator.Random(1, 100) <= 49)
                {
                    OnAddTarget(new Target(FindRandomPoint(45, 80, 70, 280) + new Vector2f(0, 0), TargetType.Left, Time.FromSeconds(3), Time.FromSeconds(3.5f)));
                }
                else
                {
                    OnAddTarget(new Target(FindRandomPoint(-80, -45, 70, 280) + new Vector2f(0, 600), TargetType.Left, Time.FromSeconds(3), Time.FromSeconds(3.5f)));
                }
            }
            if (_rightrespawn <= Time.Zero)
            {
                _rightrespawn = Time.FromSeconds(RandomGenerator.Random(8, 15));
                if (RandomGenerator.Random(1, 100) <= 49)
                {
                    OnAddTarget(new Target(FindRandomPoint(100, 135, 70, 280) + new Vector2f(700, 0), TargetType.Right, Time.FromSeconds(3), Time.FromSeconds(3.5f)));
                }
                else
                {
                    OnAddTarget(new Target(FindRandomPoint(-135, -100, 70, 280) + new Vector2f(700, 600), TargetType.Right, Time.FromSeconds(3), Time.FromSeconds(3.5f)));
                }
            }
            _switchtimer -= DeltaTime;
            if (_switchtimer <= Time.Zero)
            {
                _switchtimer = Time.FromSeconds(RandomGenerator.Random(10, 15));
                _isspace = !_isspace;
                if (_isspace) _gravity.Type = GravityType.Pull;
                else _gravity.Type = GravityType.Push;
            }
        }
        private static Vector2f FindRandomPoint(float FirstAngle, float SecondAngle, float MinLength, float MaxLength)
        {
            float angle = RandomGenerator.Random(FirstAngle, SecondAngle);
            float length = RandomGenerator.Random(MinLength, MaxLength);
            return new Vector2f((float)Trigonometry.Cos(angle) * length, (float)Trigonometry.Sin(angle) * length);
        }
        #endregion
    }
}

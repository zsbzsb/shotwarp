﻿using System;
using SFML.Window;
using NetEXT.TimeFunctions;

namespace ShotWarp.Levels
{
    public class Blank : Level
    {
        #region Variables
        #endregion

        #region Properties
        public override int RequiredLeftTargets
        {
            get
            {
                return 0;
            }
        }
        public override int RequiredRightTargets
        {
            get
            {
                return 0;
            }
        }
        public override int RequiredSpaceTargets
        {
            get
            {
                return 0;
            }
        }
        public override int TotalAmmo
        {
            get
            {
                return 0;
            }
        }
        #endregion

        #region Functions
        public override void Initialize()
        {

        }
        public override void Update(Time DeltaTime)
        {
            
        }
        #endregion
    }
}

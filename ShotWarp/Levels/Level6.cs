﻿using System;
using SFML.Window;
using NetEXT.TimeFunctions;
using NetEXT.MathFunctions;

namespace ShotWarp.Levels
{
    public class Level6 : Level
    {
        #region Variables
        private Time _warpelapsed = Time.Zero;
        private Target _left1target = null;
        private Target _right1target = null;
        private bool _left1down = true;
        private bool _right1down = false;
        private Target _left2target = null;
        private Target _right2target = null;
        private bool _left2down = true;
        private bool _right2down = false;
        #endregion

        #region Properties
        public override int RequiredLeftTargets
        {
            get
            {
                return 6;
            }
        }
        public override int RequiredRightTargets
        {
            get
            {
                return 6;
            }
        }
        public override int RequiredSpaceTargets
        {
            get
            {
                return 0;
            }
        }
        public override int TotalAmmo
        {
            get
            {
                return 30;
            }
        }
        #endregion

        #region Functions
        public override void Initialize()
        {
            OnSetLeftWarpTime(Time.FromSeconds((int)RandomGenerator.Random(1, 3)));
            OnSetRightWarpTime(Time.FromSeconds((int)RandomGenerator.Random(1, 3)));
            _left1target = new Target(new Vector2f(70, 70), TargetType.Left, Time.Zero, Time.Zero);
            OnAddTarget(_left1target);
            _right1target = new Target(new Vector2f(630, 300), TargetType.Right, Time.Zero, Time.Zero);
            OnAddTarget(_right1target);
            _left2target = new Target(new Vector2f(140, 70), TargetType.Left, Time.Zero, Time.Zero);
            OnAddTarget(_left2target);
            _right2target = new Target(new Vector2f(560, 300), TargetType.Right, Time.Zero, Time.Zero);
            OnAddTarget(_right2target);
        }
        public override void Update(Time DeltaTime)
        {
            _warpelapsed += DeltaTime;
            if (_warpelapsed >= Time.FromSeconds(10))
            {
                _warpelapsed = Time.Zero;
                OnSetLeftWarpTime(Time.FromSeconds((int)RandomGenerator.Random(1, 3)));
                OnSetRightWarpTime(Time.FromSeconds((int)RandomGenerator.Random(1, 3)));
            }

            if (_left1down) _left1target.Move(new Vector2f(0, 20 * (float)DeltaTime.Seconds));
            else _left1target.Move(new Vector2f(0, -20 * (float)DeltaTime.Seconds));
            if (_right1down) _right1target.Move(new Vector2f(0, 20 * (float)DeltaTime.Seconds));
            else _right1target.Move(new Vector2f(0, -20 * (float)DeltaTime.Seconds));
            if (_left1target.Position.Y < 70) _left1down = true;
            else if (_left1target.Position.Y > 530) _left1down = false;
            if (_right1target.Position.Y < 70) _right1down = true;
            else if (_right1target.Position.Y > 530) _right1down = false;

            if (_left2down) _left2target.Move(new Vector2f(0, 35 * (float)DeltaTime.Seconds));
            else _left2target.Move(new Vector2f(0, -35 * (float)DeltaTime.Seconds));
            if (_right2down) _right2target.Move(new Vector2f(0, 35 * (float)DeltaTime.Seconds));
            else _right2target.Move(new Vector2f(0, -35 * (float)DeltaTime.Seconds));
            if (_left2target.Position.Y < 140) _left2down = true;
            else if (_left2target.Position.Y > 460) _left2down = false;
            if (_right2target.Position.Y < 140) _right2down = true;
            else if (_right2target.Position.Y > 460) _right2down = false;
        }
        #endregion
    }
}

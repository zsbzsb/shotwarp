﻿using System;
using SFML.Window;
using NetEXT.TimeFunctions;
using NetEXT.MathFunctions;

namespace ShotWarp.Levels
{
    public class Level3 : Level
    {
        #region Variables
        private Time _warpelapsed = Time.Zero;
        private Time _targetelapsed = Time.Zero;
        #endregion

        #region Properties
        public override int RequiredLeftTargets
        {
            get
            {
                return 0;
            }
        }
        public override int RequiredRightTargets
        {
            get
            {
                return 0;
            }
        }
        public override int RequiredSpaceTargets
        {
            get
            {
                return 2;
            }
        }
        public override int TotalAmmo
        {
            get
            {
                return 0;
            }
        }
        #endregion

        #region Functions
        public override void Initialize()
        {
            OnSetLeftWarpTime(Time.FromSeconds((int)RandomGenerator.Random(1, 3)));
            OnSetRightWarpTime(Time.FromSeconds((int)RandomGenerator.Random(1, 3)));
            OnSetSpaceWarpTime(Time.FromSeconds((int)RandomGenerator.Random(1, 3)));
            OnSetLeftGravity(new Gravity(new Vector2f(0, 200)) { Type = GravityType.Push });
            OnSetRightGravity(new Gravity(new Vector2f(700, 400)) { Type = GravityType.Push });
            OnAddTarget(new Target(new Vector2f(300, 40), TargetType.Space, Time.FromSeconds(5), Time.FromSeconds(15)));
        }
        public override void Update(Time DeltaTime)
        {
            _warpelapsed += DeltaTime;
            if (_warpelapsed >= Time.FromSeconds(10))
            {
                _warpelapsed = Time.Zero;
                OnSetLeftWarpTime(Time.FromSeconds((int)RandomGenerator.Random(1, 3)));
                OnSetRightWarpTime(Time.FromSeconds((int)RandomGenerator.Random(1, 3)));
                OnSetSpaceWarpTime(Time.FromSeconds((int)RandomGenerator.Random(1, 3)));
            }
            _targetelapsed += DeltaTime;
            if (_targetelapsed >= Time.FromSeconds(25))
            {
                _targetelapsed = Time.Zero;
                OnAddTarget(new Target(new Vector2f(300, 40), TargetType.Space, Time.FromSeconds(5), Time.FromSeconds(15)));
            }
        }
        #endregion
    }
}

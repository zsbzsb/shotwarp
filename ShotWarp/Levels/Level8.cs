﻿using System;
using SFML.Window;
using NetEXT.TimeFunctions;
using NetEXT.MathFunctions;

namespace ShotWarp.Levels
{
    public class Level8 : Level
    {
        #region Variables
        private bool _isspace = false;
        private Gravity _gravity = null;
        private Time _switchtimer = Time.Zero;
        private Time _leftrespawn = Time.Zero;
        private Time _rightrespawn = Time.Zero;
        private Time _spacerespawn = Time.Zero;
        #endregion

        #region Properties
        public override int RequiredLeftTargets
        {
            get
            {
                return 2;
            }
        }
        public override int RequiredRightTargets
        {
            get
            {
                return 2;
            }
        }
        public override int RequiredSpaceTargets
        {
            get
            {
                return 2;
            }
        }
        public override int TotalAmmo
        {
            get
            {
                return 30;
            }
        }
        #endregion

        #region Functions
        public override void Initialize()
        {
            _gravity = new Gravity(new Vector2f(350, 20), 25) { Type = GravityType.Push };
            _switchtimer = Time.FromSeconds(RandomGenerator.Random(20, 30));
            OnSetGravityState(GravityState.Space);
            OnSetSpaceGravity(_gravity);
            OnSetLeftWarpTime(Time.FromSeconds(3));
            OnSetRightWarpTime(Time.FromSeconds(3));
            OnSetSpaceWarpTime(Time.FromSeconds(4.5f));
        }
        public override void Update(Time DeltaTime)
        {
            if (!_isspace)
            {
                _leftrespawn -= DeltaTime;
                _rightrespawn -= DeltaTime;
                if (_leftrespawn <= Time.Zero)
                {
                    _leftrespawn = Time.FromSeconds(RandomGenerator.Random(8, 15));
                    if (RandomGenerator.Random(1, 100) <= 49)
                    {
                        OnAddTarget(new Target(FindRandomPoint(45, 80, 70, 280) + new Vector2f(0, 0), TargetType.Left, Time.FromSeconds(6), Time.FromSeconds(3)));
                    }
                    else
                    {
                        OnAddTarget(new Target(FindRandomPoint(-80, -45, 70, 280) + new Vector2f(0, 600), TargetType.Left, Time.FromSeconds(6), Time.FromSeconds(3)));
                    }
                }
                if (_rightrespawn <= Time.Zero)
                {
                    _rightrespawn = Time.FromSeconds(RandomGenerator.Random(8, 15));
                    if (RandomGenerator.Random(1, 100) <= 49)
                    {
                        OnAddTarget(new Target(FindRandomPoint(100, 135, 70, 280) + new Vector2f(700, 0), TargetType.Right, Time.FromSeconds(6), Time.FromSeconds(3)));
                    }
                    else
                    {
                        OnAddTarget(new Target(FindRandomPoint(-135, -100, 70, 280) + new Vector2f(700, 600), TargetType.Right, Time.FromSeconds(6), Time.FromSeconds(3)));
                    }
                }
            }
            else
            {
                _spacerespawn -= DeltaTime;
                if (_spacerespawn <= Time.Zero)
                {
                    _spacerespawn = Time.FromSeconds(RandomGenerator.Random(15, 20));
                    OnAddTarget(new Target(new Vector2f(RandomGenerator.RandomDev(350, 100), RandomGenerator.RandomDev(100, 25)), TargetType.Space, Time.FromSeconds(8.5), Time.FromSeconds(6.5)));
                }
            }
            _switchtimer -= DeltaTime;
            if (_switchtimer <= Time.Zero)
            {
                _switchtimer = Time.FromSeconds(RandomGenerator.Random(20, 30));
                _leftrespawn = Time.Zero;
                _rightrespawn = Time.Zero;
                _spacerespawn = Time.Zero;
                _isspace = !_isspace;
                if (_isspace) _gravity.Type = GravityType.Pull;
                else _gravity.Type = GravityType.Push;
            }
        }
        private static Vector2f FindRandomPoint(float FirstAngle, float SecondAngle, float MinLength, float MaxLength)
        {
            float angle = RandomGenerator.Random(FirstAngle, SecondAngle);
            float length = RandomGenerator.Random(MinLength, MaxLength);
            return new Vector2f((float)Trigonometry.Cos(angle) * length, (float)Trigonometry.Sin(angle) * length);
        }
        #endregion
    }
}

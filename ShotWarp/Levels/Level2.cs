﻿using System;
using SFML.Window;
using NetEXT.TimeFunctions;

namespace ShotWarp.Levels
{
    public class Level2 : Level
    {
        #region Variables
        #endregion

        #region Properties
        public override int RequiredLeftTargets
        {
            get
            {
                return 1;
            }
        }
        public override int RequiredRightTargets
        {
            get
            {
                return 2;
            }
        }
        public override int RequiredSpaceTargets
        {
            get
            {
                return 0;
            }
        }
        public override int TotalAmmo
        {
            get
            {
                return 0;
            }
        }
        #endregion

        #region Functions
        public override void Initialize()
        {
            OnAddTarget(new Target(new Vector2f(575, 250), TargetType.Right, Time.Zero, Time.Zero));
            OnAddTarget(new Target(new Vector2f(125, 200), TargetType.Left, Time.Zero, Time.Zero));
            OnAddTarget(new Target(new Vector2f(30, 500), TargetType.Left, Time.Zero, Time.Zero));
            OnSetLeftGravity(new Gravity(new Vector2f(10, 400), 40));
            OnSetRightGravity(new Gravity(new Vector2f(700, 400)));
        }
        public override void Update(Time DeltaTime)
        {
            
        }
        #endregion
    }
}

﻿using System;
using SFML.Window;

namespace ShotWarp
{
    public class Gravity
    {
        #region Variables
        private Vector2f _position = new Vector2f();
        private float _strength = 20f;
        private GravityType _type = GravityType.Pull;
        #endregion

        #region Properties
        public Vector2f Position
        {
            get 
            {
                return _position;
            }
            set
            {
                _position = value;
            }
        }
        public float Strength
        {
            get
            {
                return _strength;
            }
            set
            {
                _strength = value;
            }
        }
        public GravityType Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
            }
        }
        #endregion

        #region Constructors
        public Gravity(Vector2f Position, float Strength = 30)
        {
            _position = Position;
            _strength = Strength;
        }
        #endregion
    }
}
